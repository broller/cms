# CMS Framework #

A personal CMS framework that was used as the basis for creating a [personal website](http://www.brandonroller.com/).  Framework is built around the concept that multiple users can post to the same site.  Users can log-in, post articles, and also edit or delete articles they have previously written.