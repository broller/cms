module ApplicationHelper
	def renderText(text)
		#Redcarpet::Render::HTML
		markdown = Redcarpet::Markdown.new(CustomMarkdown, no_intra_emphasis: true)
		return markdown.render(text).html_safe
	end
end
