class ArticlesController < ApplicationController
	before_action :authenticate_user!, only: [:new, :create, :edit, :update, :destroy]

	def index
		if(params[:id].present?)
			@articles = Article.where(user_id:params[:id])
		else
			@articles = Article.where(user_id:current_user.id)
		end
	end

	def show
		@article = Article.find(params[:id])
	end

	def new
		@article = Article.new
		@article.tags.build
		@article.article_images.build
	end

	def create
		@article = current_user.articles.build(article_params)
		if @article.save
			flash[:success] = "Article uploaded"
			redirect_to root_url
		else
			flash[:error] = @article.errors.full_messages.to_sentence
			render 'new'
		end
	end

	def edit
		@article = Article.find(params[:id])
	end

	def update
		@article = Article.find(params[:id])
		if @article.update_attributes(article_params)
			flash[:success] = "Article updated"
      		redirect_to @article
		else
			render 'edit'
		end
	end

	def destroy
		Article.destroy(params[:id])
    	redirect_to root_url
	end

	private
		def article_params
			params.require(:article).permit(:title, :content, tags_attributes: [:id, keyword_attributes: [:keyword]], article_images_attributes: [:id, :image])
		end

end
