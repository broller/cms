
class StaticPagesController < ApplicationController
  def home
  	@articles = Article.all.order('created_at DESC').paginate(:page => params[:page], :per_page => 10)
  	
  	if @articles.size > 3
  		@carousel_articles = @articles.slice(0, 3)
  	else
		@carousel_articles = @articles
  	end
  end
end
