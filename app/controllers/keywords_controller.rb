class KeywordsController < ApplicationController
	def index
		@keyword = Keyword.find(params[:id])
		@articles = Article.includes(:tags).where( tags: {keyword: params[:id]})
	end
end
