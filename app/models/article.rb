class Article < ActiveRecord::Base
  before_create :downcase_tags

  belongs_to :user
  validates :user, :presence => true

  has_many :article_images, dependent: :destroy
  has_many :tags
  has_many :keywords, :through => :tags

  validates :title, presence: true, uniqueness: true
  validates :content, length: { minimum: 1, maximum: 10000,
          too_short: "%{count} characters is the minimum allowed",
          too_long: "%{count} characters is the maximum allowed" }

  accepts_nested_attributes_for :article_images
  accepts_nested_attributes_for :tags, reject_if: :all_blank, allow_destroy: true
  accepts_nested_attributes_for :keywords

  def author
  	self.user.first_name + " " + self.user.last_name
  end


  def downcase_tags
      self.tags.each do |tag|
       
        tag.keyword.keyword = tag.keyword.keyword.downcase 
      end
  end

end
