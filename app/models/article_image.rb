class ArticleImage < ActiveRecord::Base
	mount_uploader :image, ImageUploader
    belongs_to :article, autosave: true
end
