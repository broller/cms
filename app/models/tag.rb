class Tag < ActiveRecord::Base
	belongs_to :article
	belongs_to :keyword, autosave: true

	accepts_nested_attributes_for :keyword,
                              :reject_if => :all_blank

	private
    	def autosave_associated_records_for_keyword
    		if new_keyword = Keyword.find_by_keyword(keyword.keyword)
    			self.keyword = new_keyword
            else
                keyword.save
                self.keyword = keyword
    		end
    	end
end