$(document).ready(function(){
  $( "#bold" ).click(function() {
      addEmphasis("*");
    });

    $( "#italic" ).click(function() {
      addEmphasis("**");
    });

    $( "#underline" ).click(function() {
      addEmphasis("_");
    });

    $( "#url" ).click(function() {
      addURL();
    });

    $( "#blockquote" ).click(function() {
      addBlockQuote();
    });

    $("#ul").click(function(){
      addListItem("*");
    });

    $("#ol").click(function(){
      addListItem(".");
    });
});
	
	function addEmphasis(emphasis){
  		var textComponent = getTextArea();
  		var selectedText;
 
  		// Mozilla version
  		if (textComponent.selectionStart != undefined &&
  			textComponent.selectionStart != textComponent.selectionEnd) {
    		var startPos = textComponent.selectionStart;
    		var endPos = textComponent.selectionEnd;
    		selectedText = textComponent.value.substring(startPos, endPos);
    		var newText = textComponent.value.substring(0, startPos) + emphasis + textComponent.value.substring(startPos, endPos) + emphasis + textComponent.value.substring(endPos);
			 textComponent.value = newText;
  		}
  	}

    function addListItem(listType){
      var textComponent = getTextArea();
      var selectedText;

      // Mozilla version
      if (textComponent.selectionStart != undefined &&
        textComponent.selectionStart != textComponent.selectionEnd) {
        
        var startPos = textComponent.selectionStart;
        var endPos = textComponent.selectionEnd;
        
        var newText = textComponent.value.substring(0, startPos);
        selectedText = textComponent.value.substring(startPos, endPos).split(/\r\n|\r|\n/g);
        
        newText += listType == "." ? orderedList(selectedText,listType) : unorderedList(selectedText,listType);

        newText += textComponent.value.substring(endPos);
        textComponent.value = newText;
      }      
    }

    function unorderedList(selectedText, listType){
      var newText = "";
      for(var i = 0; i < selectedText.length; i++){
          if(i < selectedText.length - 1)
            newText += listType + selectedText[i] + "\r\n";
          else
            newText += listType + selectedText[i];
        }

        return newText;
    }

    function orderedList(selectedText,listType){
        var newText = "";
        for(var i = 0; i < selectedText.length; i++){
          if(i < selectedText.length - 1)
            newText += (i + 1) + listType + selectedText[i] + "\r\n";
          else
            newText += (i + 1) + listType + selectedText[i];
        }
        return newText;
    }
 
 	function addURL(){
  		var textComponent = getTextArea();
  		var selectedText;

  		if(textComponent.selectionStart != undefined){
  			if(textComponent.selectionStart != textComponent.selectionEnd){
  				var startPos = textComponent.selectionStart;
    			var endPos = textComponent.selectionEnd;
    			selectedText = textComponent.value.substring(startPos, endPos);
    			newText = textComponent.value.substring(0, startPos) + "[" + textComponent.value.substring(startPos, endPos) + "]()" + textComponent.value.substring(endPos);
				textComponent.value = newText;
  			}
  			else {
  				var startPos = textComponent.selectionStart;
  				newText = textComponent.value.substring(0, startPos) + "[]()" + textComponent.value.substring(startPos);
				textComponent.value = newText;
  			}
  		}
  	}

  	function addBlockQuote(){
  		var textComponent = getTextArea();
  		var selectedText;

  		if(textComponent.selectionStart != undefined){
  			if(textComponent.selectionStart != textComponent.selectionEnd){
  				var startPos = textComponent.selectionStart;
    			var endPos = textComponent.selectionEnd;
    			selectedText = textComponent.value.substring(startPos, endPos);
    			newText = textComponent.value.substring(0, startPos) + "\r\n\r\n\>" + textComponent.value.substring(startPos);
				textComponent.value = newText;
  			}
  			else {
  				var startPos = textComponent.selectionStart;
  				newText = textComponent.value.substring(0, startPos) + "\r\n\r\n\>" + textComponent.value.substring(startPos);
				textComponent.value = newText;
  			}
  		}	
  	}

	function getTextArea(){
  		return document.getElementById('article_content');
  	}