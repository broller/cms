class CreateKeywords < ActiveRecord::Migration
  def change
    create_table :keywords do |t|
      t.string :keyword

      t.timestamps null: false
    end

    create_table :articles_parts do |t|
    	t.belongs_to :article, index: true
    	t.belongs_to :keywords, index: true
    end
  end
end
