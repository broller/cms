class CreateTags < ActiveRecord::Migration
  def change
    create_table :tags do |t|
      t.belongs_to :article, index: true
      t.belongs_to :keyword, index: true

      t.timestamps null: false
    end

  end
end
